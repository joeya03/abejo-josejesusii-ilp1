using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    //declaring variables

    //grabbing game components
    public GameObject diamondprefab;
    public GameObject character;
    public Transform[] locations = new Transform[5];
    public Text pointsSystem;

    //static components
    private static GameObject diamond, staticDiamond;
    private static Transform[] locationStatic = new Transform[5];
    public static Text pointTally;

    static int score = 0;



    // Start is called before the first frame update
    void Start()
    {
        //set static components
        staticDiamond = diamondprefab;
        locationStatic = locations;
        pointTally = pointsSystem;

        //creates a diamond from the start
        diamond = Instantiate(diamondprefab, locations[Random.Range(0, 5)].position, Quaternion.identity);
    }

    //spawns diamonds when collected
    public static void spawnDiamond() 
    {
        //create diamond
        diamond = Instantiate(staticDiamond, locationStatic[Random.Range(0, 5)].position, Quaternion.identity);

        //add score and update score txt
        score += 50;
        pointTally.text = "Score: " + score;
    }
}
