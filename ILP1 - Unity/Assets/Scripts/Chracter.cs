using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chracter : MonoBehaviour
{
    //Declaring Variables

    //components
    Rigidbody2D rb;
    Animator anim;
    public Transform groundCheck;

    //movement
    private Vector2 movement;
    public float speed;
    public float jumpForce;
    bool isGrounded = false;
    bool facingRight = true;

    //ground check
    public float groundRadius;
    public LayerMask groundLayer;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }//end of Start

    // Update is called once per frame
    void Update()
    {
        Movement();
        Jump();
    }//end of update

    // Moving function
    void Movement()
    {
        //grabs players "Horizontal" input for left/right movement
        movement.x = Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime;

        anim.SetFloat("speedX", Mathf.Abs(Input.GetAxisRaw("Horizontal")));
        anim.SetFloat("yValue", rb.velocity.y);

        //flips player model according to directions
        if (movement.x > 0f && !facingRight)
            Flip();//flip right when looking left
        else if (movement.x < 0f && facingRight)
            Flip();//flip left hen looking right

        ////translates movement to character(used translate for class purpose)
        transform.Translate(movement.x, 0f, 0f);
        rb.velocity = new Vector2(movement.x, rb.velocity.y);

    }//end of Move

    //flips player
    void Flip()
    {
        facingRight = !facingRight;

        //turns player model
        Vector3 theScale = transform.localScale;
        theScale.x *= -1f;

        transform.localScale = theScale;
    }

    // Jumping function
    void Jump()
    {
        //allows player to jump(used addForce for class purpose)
        if (Input.GetButtonDown("Jump") && CheckIfGrounded())
            rb.AddForce(new Vector2(0, jumpForce));

        ////allows player to jump
        //if (Input.GetButtonDown("Jump") && isGrounded)
        //    rb.velocity = new Vector2(rb.velocity.x, jumpForce);

        ////can let go of jump 
        //if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        //    rb.velocity = new Vector2(rb.velocity.x, jumpForce * 0.5f);

    }//end of Jump

    //checks if grounded
    private bool CheckIfGrounded()
    {
        Collider2D collider = Physics2D.OverlapCircle(groundCheck.position, groundRadius, groundLayer);

        return isGrounded = (collider != null);

    }//end of CheckIfGrounded

}//end of class
