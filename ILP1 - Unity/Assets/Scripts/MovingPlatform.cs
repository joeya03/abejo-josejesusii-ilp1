using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    //declaring variables
    public Transform startingPosition, endPosition;
    bool ending = false;
    public float speed = 1.0f;

    // Update is called once per frame
    void Update()
    {
        //if platform is ending, move towards starting position, else move towards end position(both position referced with empty objects)
        if (!ending)
            transform.position = Vector2.MoveTowards(transform.position, endPosition.position, Time.deltaTime * speed);
        else
            transform.position = Vector2.MoveTowards(transform.position, startingPosition.position, Time.deltaTime * speed);

        //used for horizotal movement platforms
        if (startingPosition.position.x == endPosition.position.x)
        {
            if (transform.position.y == endPosition.position.y)
                ending = true;
            else if (transform.position.y == startingPosition.position.y)
                ending = false;
        }
        //used for veritcal movement platforms
        else if (startingPosition.position.y == endPosition.position.y) 
        {
            if (transform.position.x == endPosition.position.x)
                ending = true;
            else if (transform.position.x == startingPosition.position.x)
                ending = false;
        }

    }//end of update

    //onCollisionEnter event
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if the player collides with the platform, then add the player to be a child from the platform
        if (collision.gameObject.tag == "Player")
            collision.gameObject.transform.SetParent(transform);
    }

    //onCollisionExit event
    private void OnCollisionExit2D(Collision2D collision)
    {
        //if the player leaves the platform, then set the player to null(resulting on having no parent)
        if (collision.gameObject.tag == "Player")
            collision.gameObject.transform.SetParent(null);
    }

}//end of class
