using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour
{
    public bool onScreen = true;

    //onTriggerEnter event
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player touches the diamond, destory gameobject, and create new diamond
        if (collision.gameObject.tag == "Player")
        {
            collision.GetComponent<AudioSource>().Play();

            GameManager.spawnDiamond();

            Destroy(gameObject);
        }
    }
}
